/*
 * lps22hb_core.h
 *
 *  Created on: 02 march 2021
 *      Author: schuluka
 */

#ifndef LPS22HB_LPS22HB_CORE_H_
#define LPS22HB_LPS22HB_CORE_H_

#include "lps22hb.h"

/*! \addtogroup lps22hb_core_macros
*  LPS22HB_CORE macros
*  @{
*/

/**
  * Compare a byte of two configurations against each other
  *
  * @param  config1  configuration struct 1 (ptr)
  * @param  config2  configuration struct 2 (ptr)
  * @param  i        byte to compare
  * @retval uint8_t  0 if bytes match, else 1
  *
**/
#define LPS22HB_COMPARE_STRUCT_BYTE(config1, config2, i) (*(((uint8_t *) config1) + i) != *(((uint8_t *) config2) + i))

/*! \addtogroup lps22hb_core_functions
*  LPS22HB_CORE functions
*  @{
*/

/**
  * Get configuration registers from the sensor
  *
  * @param  comm    communication struct defined from the user (ptr)
  * @param  config  configuration struct where to write configuration registers (ptr)
  *
**/
void get_config_registers (lps22hb_communication_t * comm,
			   lps22hb_configuration_t * config);

/**
  * Set user configuration in the sensor
  *
  * @param  comm    communication struct defined from the user (ptr)
  * @param  config  configuration struct where the configurations are saved (ptr)
  *
**/
void set_config_registers (lps22hb_communication_t * comm,
			   lps22hb_configuration_t * config);

/**
  * Compare to configurations for the sensor
  *
  * @param  config1 first configs struct (ptr)
  * @param  config2 second configs struct (ptr)
  * @retval 0 if structs match, 1 otherwise
  *
**/
uint8_t
compare_configs (lps22hb_configuration_t * config1,
		 lps22hb_configuration_t * config2);

/*! @} */

#endif /* LPS22HB_LPS22HB_CORE_H_ */
