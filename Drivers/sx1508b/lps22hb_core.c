/*
 * sx1508b_core.c
 *
 *  Created on: 03 march
 *      Author: schuluka
 */
#include "lps22hb_core.h"


/**
  * Get configuration registers from the sensor
  *
  * @param  comm    communication struct defined from the user (ptr)
  * @param  config  configuration struct where to write configuration registers (ptr)
  *
**/
void
get_config_registers (lps22hb_communication_t * comm,
		      lps22hb_configuration_t * config)
{
  uint8_t reg = 0;

  // Read Interrupt control register 
  comm->read (LPS22HB_INT_CFG, &reg, 1);
  // Store register configuration in struct
  config->interrupt_gfc.autorifp = (reg >> 7) & 0x01;
  config->interrupt_gfc.reset_arp = (reg >> 6) & 0x01;
  config->interrupt_gfc.autozero = (reg >> 5) & 0x01;
  config->interrupt_gfc.reset_az = (reg >> 4) & 0x01;
  config->interrupt_gfc.diff_en = (reg >> 3) & 0x01;
  config->interrupt_gfc.lir = (reg >> 2) & 0x01;
  config->interrupt_gfc.ple = (reg >> 1) & 0x01;
  config->interrupt_gfc.phe = reg & 0x01;

  // Read control register 1
  comm->read (LPS22HB_CTRL_REG1, &reg, 1);
  // Store register configuration in struct
  config->ctrl_reg1.odr = (reg >> 4) & 0x07;
  config->ctrl_reg1.en_lpfp = (reg >> 3) & 0x01;
  config->ctrl_reg1.lpfp_cfg = (reg >> 2) & 0x01;
  config->ctrl_reg1.bdu = (reg >> 1) & 0x01;
  config->ctrl_reg1.sim = reg & 0x01;

  // Read control register 2
  comm->read (LPS22HB_CTRL_REG2, &reg, 1);
  // Store register configuration in struct
  config->ctrl_reg2.boot = (reg >> 7) & 0x01;
  config->ctrl_reg2.fifo_en = (reg >> 6) & 0x01;
  config->ctrl_reg2.stop_on_fth = (reg >> 5) & 0x01;
  config->ctrl_reg2.if_add_inc = (reg >> 4) & 0x01;
  config->ctrl_reg2.i2c_dis = (reg >> 3) & 0x01;
  config->ctrl_reg2.swreset = (reg >> 2) & 0x01;
  config->ctrl_reg2.one_shot = reg & 0x01;

  // Read control register 3
  comm->read (LPS22HB_CTRL_REG3, &reg, 1);
  // Store register configuration in struct
  config->ctrl_reg3.int_h_l = (reg >> 7) & 0x01;
  config->ctrl_reg3.pp_od = (reg >> 6) & 0x01;
  config->ctrl_reg3.f_fss5 = (reg >> 5) & 0x01;
  config->ctrl_reg3.f_fth = (reg >> 4) & 0x01;
  config->ctrl_reg3.f_ovr = (reg >> 3) & 0x01;
  config->ctrl_reg3.drdy = (reg >> 2) & 0x01;
  config->ctrl_reg3.int_s = reg & 0x03;
}

/**
  * Set user configuration in the sensor
  *
  * @param  comm    communication struct defined from the user (ptr)
  * @param  config  configuration struct where the configurations are saved (ptr)
  *
**/
void
set_config_registers (lps22hb_communication_t * comm,
		      lps22hb_configuration_t * config)
{
  uint8_t reg = 0;

  // Write Interrupt control register 
  reg = (config->interrupt_gfc.autorifp << 7) |
    (config->interrupt_gfc.reset_arp << 6) |
    (config->interrupt_gfc.autozero << 5) |
    (config->interrupt_gfc.reset_az << 4) |
    (config->interrupt_gfc.diff_en << 3) |
    (config->interrupt_gfc.lir << 2) |
    (config->interrupt_gfc.ple << 1) | (config->interrupt_gfc.phe);

  comm->write (LPS22HB_INT_CFG, &reg);

  // Write control register 1
  reg = 0;
  reg = (config->ctrl_reg1.odr << 4) |
    (config->ctrl_reg1.en_lpfp << 3) |
    (config->ctrl_reg1.lpfp_cfg << 2) |
    (config->ctrl_reg1.bdu << 1) | (config->ctrl_reg1.sim);

  comm->write (LPS22HB_CTRL_REG1, &reg);

  // Write control register 2
  reg = 0;
  reg = (config->ctrl_reg2.boot << 7) |
    (config->ctrl_reg2.fifo_en << 6) |
    (config->ctrl_reg2.stop_on_fth << 5) |
    (config->ctrl_reg2.if_add_inc << 4) |
    (config->ctrl_reg2.i2c_dis << 3) |
    (config->ctrl_reg2.swreset << 2) | (config->ctrl_reg2.one_shot);

  comm->write (LPS22HB_CTRL_REG2, &reg);

  // Write control register 3
  reg = 0;
  reg = (config->ctrl_reg3.int_h_l << 7) |
    (config->ctrl_reg3.pp_od << 6) |
    (config->ctrl_reg3.f_fss5 << 5) |
    (config->ctrl_reg3.f_fth << 4) |
    (config->ctrl_reg3.f_ovr << 3) |
    (config->ctrl_reg3.drdy << 2) | (config->ctrl_reg3.int_s);

  comm->write (LPS22HB_CTRL_REG3, &reg);
}

/**
  * Compare two configurations for the sensor
  *
  * @param  config1 first configs struct (ptr)
  * @param  config2 second configs struct (ptr)
  * @retval 0 if structs match, 1 otherwise
  *
**/
uint8_t
compare_configs (lps22hb_configuration_t * config1,
		 lps22hb_configuration_t * config2)
{
  uint8_t ret_val = 0;

  for (uint32_t i = 0;
       i < sizeof (lps22hb_configuration_t) / sizeof (uint8_t); i++)
    {
      ret_val = ret_val || LPS22HB_COMPARE_STRUCT_BYTE (config1, config2, i);
    }

  return ret_val;
}
