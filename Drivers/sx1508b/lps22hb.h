/*
 * lps22hb.h
 *
 *  Created on: 03 march 2021
 *      Author: schuluka
 */

#ifndef LPS22HB_DRIVER_LPS22HB_H_
#define LPS22HB_DRIVER_LPS22HB_H_




#include <stdint.h>		//may not be ideal depending on linking of implementation, maybe add as requirement?


/*! \addtogroup lps22hb_defines
*  LPS22HB defines
*  @{
*/

/* Read and write */

#define LPS22HB_SA0_HIGH			0x00	/**< Last bit of the address (SAD[0]) defined by SA0 pin**/
#define LPS22HB_SA0_LOW				0x02	/**< Last bit of the address (SAD[0]) defined by SA0 pin**/

#define LPS22HB_READ_ADDRESS_BASE 	0xB9	/**< Read address without SAD[0]**/
#define LPS22HB_WRITE_ADDRESS_BASE	0xB8	/**< Write address without SAD[0]**/

/*! @} */

/*! \addtogroup lps22hb_registers
*  LPS22HB registers
*  @{
*/

/*********** REGISTERS ***********/
/* Setup registers */
#define LPS22HB_CTRL_REG1		0x10	/**< Control register 1 **/
#define LPS22HB_CTRL_REG2		0x11	/**< Control register 2 **/
#define LPS22HB_CTRL_REG3		0x12	/**< Control register 3 **/

#define LPS22HB_INT_CFG			0x0B	/**< Interrupt control register **/
#define LPS22HB_INT_SOURCE		0x25	/**< Interrupt source register **/

#define LPS22HB_THS_P_L			0x0C	/**< Threshold for Interrupt (low byte)**/
#define LPS22HB_THS_P_H			0x0D	/**< Threshold for Interrupt (high byte)**/
#define LPS22HB_WHO_AM_I		0x0F	/**< Device identification register **/

#define LPS22HB_FIFO_CTRL		0x14	/**< FIFO control register **/
#define LPS22HB_FIFO_STATUS		0x26	/**< FIFO status register **/

#define LPS22HB_RES_CONF		0x1A	/**< Low-power mode register **/
#define LPS22HB_LPFP_RES		0x33	/**< Low-pass filter reset register **/

#define LPS22HB_STATUS			0x27	/**< Status register **/


/* Calibration registers **/
#define LPS22HB_REF_P_XL		0x15	/**< Reference pressure register (low byte) **/
#define LPS22HB_REF_P_L			0x16	/**< Reference pressure register (middle byte) **/
#define LPS22HB_REF_P_H			0x17	/**< Reference pressure register (high byte) **/

#define LPS22HB_RPDS_L			0x18	/**< Pressure offser register (low byte) **/
#define LPS22HB_RPDS_H			0x19	/**< Pressure offser register (low byte) **/

/* Data registers */
#define LPS22HB_PRESS_OUT_XL	0x28	/**< Pressure output register (low byte) **/
#define LPS22HB_PRESS_OUT_L		0x29	/**< Pressure output register (middle byte) **/
#define LPS22HB_PRESS_OUT_H		0x2A	/**< Pressure output register (high byte) **/

#define LPS22HB_TEMP_OUT_L		0x2B	/**< Temperature output register (low byte) **/
#define LPS22HB_TEMP_OUT_H		0x2C	/**< Temperature output register (high byte) **/

/*! @} */

/*! \addtogroup lps22hb_settings
*  LPS22HB settings
*  @{
*/

/*********** SETTINGS DEFINITIONS ***********/

#define LPS22HB_ENABLE			0x01
#define LPS22HB_DISABLE			0x00

#define LPS22HB_WHO_AM_I_VAL    0xB1	/*<< Device Who am I value * */

#define LPS22HB_ODR_ONE_SHOT	0x00	/**< Output data rate => One-shot **/
#define LPS22HB_ODR_1HZ			0x01	/**< Output data rate => 1 Hz **/
#define LPS22HB_ODR_10HZ		0x02	/**< Output data rate => 10 Hz **/
#define LPS22HB_ODR_25HZ		0x03	/**< Output data rate => 25 Hz **/
#define LPS22HB_ODR_50HZ		0x04	/**< Output data rate => 50 Hz **/
#define LPS22HB_ODR_75HZ		0x05	/**< Output data rate => 75 Hz **/


#define LPS22HB_LPF_BW_ODR_2	0x00	/**< Low-pass filter badwidh = output data rate / 2 **/
#define LPS22HB_LPF_BW_ODR_9	0x02	/**< Low-pass filter badwidh = output data rate / 9 **/
#define LPS22HB_LPF_BW_ODR_20	0x03	/**< Low-pass filter badwidh = output data rate / 20 **/

#define LPS22HB_INT_DATA		0x00	/**< INT_DRDY pin:	Interrupt on data **/
#define LPS22HB_INT_PH			0x01	/**< INT_DRDY pin:	Interrupt on pressure high **/
#define LPS22HB_INT_PL			0x02	/**< INT_DRDY pin:	Interrupt on pressure low **/
#define LPS22HB_INT_PHL			0x03	/**< INT_DRDY pin:	Interrupt on pressure high or low **/

#define LPS22HB_FIFO_M_B		0x00	/**< FIFO mode:	Bypass **/
#define LPS22HB_FIFO_M_F		0x01	/**< FIFO mode:	FIFO **/
#define LPS22HB_FIFO_M_S		0x02	/**< FIFO mode:	Stream **/
#define LPS22HB_FIFO_M_STF		0x03	/**< FIFO mode:	Stream-to-FIFO **/
#define LPS22HB_FIFO_M_BTS		0x04	/**< FIFO mode:	Bypass-to-Stream **/
#define LPS22HB_FIFO_M_DS		0x06	/**< FIFO mode:	Dynamic-Stream **/
#define LPS22HB_FIFO_M_BTF		0x07	/**< FIFO mode:	Bypass-to-FIFO **/

/*! @} */

/*! \addtogroup lps22hb_structs
*  LPS22HB structs
*  @{
*/

/*********** STRUCTS ***********/

typedef struct
{
  uint8_t autorifp:1;
  uint8_t reset_arp:1;
  uint8_t autozero:1;
  uint8_t reset_az:1;
  uint8_t diff_en:1;
  uint8_t lir:1;
  uint8_t ple:1;
  uint8_t phe:1;
} lps22hb_interrupt_gfc_t;

typedef struct
{
  uint8_t:1;
  uint8_t odr:3;
  uint8_t en_lpfp:1;
  uint8_t lpfp_cfg:1;
  uint8_t bdu:1;
  uint8_t sim:1;
} lps22hb_ctrl_reg1_t;

typedef struct
{
  uint8_t boot:1;
  uint8_t fifo_en:1;
  uint8_t stop_on_fth:1;
  uint8_t if_add_inc:1;
  uint8_t i2c_dis:1;
  uint8_t swreset:1;
    uint8_t:1;
  uint8_t one_shot:1;
} lps22hb_ctrl_reg2_t;

typedef struct
{
  uint8_t int_h_l:1;
  uint8_t pp_od:1;
  uint8_t f_fss5:1;
  uint8_t f_fth:1;
  uint8_t f_ovr:1;
  uint8_t drdy:1;
  uint8_t int_s:2;
} lps22hb_ctrl_reg3_t;

typedef struct
{
  uint8_t f_mode:3;
  uint8_t wtm:5;

} lps22hb_fifo_ctrl_t;

typedef struct
{
  lps22hb_interrupt_gfc_t interrupt_gfc;
  lps22hb_ctrl_reg1_t ctrl_reg1;
  lps22hb_ctrl_reg2_t ctrl_reg2;
  lps22hb_ctrl_reg3_t ctrl_reg3;
} lps22hb_configuration_t;

typedef struct
{
  void (*write) (uint8_t reg, uint8_t * buf);							/**< User write function **/
  void (*read) (uint8_t reg, uint8_t * buf, uint8_t bytes);			/**< User read function **/
} lps22hb_communication_t;

typedef struct
{
  lps22hb_communication_t comm;
  lps22hb_configuration_t config;
} lps22hb_t;


typedef uint8_t lps22hb_err_code_t;
#define LPS22HB_SUCCESS                             (0x00)
#define LPS22HB_ERR_WHO_AM_I                        (0x01)
#define LPS22HB_ERR_VALIDATE_SETTINGS               (0x02)
/*! @} */

/*! \addtogroup lps22hb_functions
*  LPS2HB functions
*  @{
*/

/*********** Functions declaration ***********/

/**
  * Save the pointer of the write and read function
  * that the user defined. Also modify the config struct
  * of the user with the default variable.
  *
  * @param  configs   	user defined setup struct (ptr)
  * @retval       		interface status (MANDATORY: return 0 -> no Error) TODO
  *
**/
void lps22hb_setup_communication (lps22hb_t * configs);

/**
  * Copy the configuration variables from the
  * default configuration file.
  *
  * @param  configs   	user defined setup struct (ptr)
  * @retval       		interface status (MANDATORY: return 0 -> no Error) TODO
  *
**/
void lps22hb_default_settings (lps22hb_t * configs);

/**
  * Write the user configurations in the LPS22HB sensor
  *
  * @param  configs   	user defined setup struct (ptr)
  * @retval       	 	interface status (MANDATORY: return 0 -> no Error) TODO
  *
**/
void lps22hb_setup_sensor (lps22hb_t * configs);

/**
  * Validate the user configurations in the LPS22HB sensor
  *
  * @param  setup       user defined setup struct (ptr)
  * @retval             lps22hb_err_code_t: error code, LPS22HB_SUCCESS on success
  *
  */
lps22hb_err_code_t lps22hb_validate_sensor (lps22hb_t * configs);

/**
  * Setup and validate the user configurations in the LPS22HB sensor
  *
  * @param  setup       user defined setup struct (ptr)
  * @retval             lps22hb_err_code_t: error code, LPS22HB_SUCCESS on success
  *
  */
lps22hb_err_code_t lps22hb_setup_and_validate_sensor (lps22hb_t * configs);

/**
  * General register read command
  *
  * @param				uint8_t reg: address to the register
  * @param				uint8_t* reg: ptr to get the read value
  *
  */
void lps22hb_read_reg (uint8_t reg, uint8_t * value);

/**
  * General register write command
  *
  * @param				uint8_t reg: address to the register
  * @param				uint8_t* reg: ptr to get the read value
  *
  */
void lps22hb_write_reg (uint8_t reg, uint8_t * value);

/**
  * Get the WHO AM I register
  *
  * @param				uint8_t* reg: ptr to get the read value
  *
  */
void lps22hb_get_who_am_i (uint8_t * value);

/**
  * Check the WHO AM I register
  *
  * @retval             lps22hb_err_code_t: error code, LPS22HB_SUCCESS on success
  */
lps22hb_err_code_t lps22hb_check_who_am_i (void);

/**
  * Get the values of the three control registers
  *
  * @param				uint8_t* reg: ptr to get the read value.
  * 					need to point to a buffer of min size 3
  */
void lps22hb_control_registers (uint8_t * value);

/**
  * Get the values of the pressure registers
  *
  * @param				uint8_t* reg: ptr to get the read value.
  * 					need to point to a buffer of min size 3
  */
void lps22hb_get_raw_pressure (uint8_t * value);

/**
 * Get the pressure in hPa
 *
 * @retval				uint32_t: pressure value. *
 */
uint32_t lps22hb_get_pressure (void);

/**
  * Get the values of the pressure reference registers
  *
  * @param				uint8_t* reg: ptr to get the read value.
  * 					need to point to a buffer of min size 3
  */
void lps22hb_get_pressure_reference (uint8_t * value);

/**
  * Get the values of the pressure offset registers
  *
  * @param				uint8_t* reg: ptr to get the read value.
  * 					need to point to a buffer of min size 3
  */
void lps22hb_get_pressure_offset (uint8_t * value);

/**
  * Get the threshold of the pressure
  *
  * @param				uint8_t* reg: ptr to get the read value.
  * 					need to point to a buffer of min size 3
  */
void lps22hb_get_pressure_threshold (uint8_t * value);

/**
  * Get the values of the temperature registers
  *
  * @param				uint8_t* reg: ptr to get the read value.
  * 					need to point to a buffer of min size 2
  */
void lps22hb_get_raw_temperature (uint8_t * value);

/**
  * Get the temperature in °C
  *
  * @retval				int16_t: temperature value.
  */
int16_t lps22hb_get_temperature (void);

#endif /* LPS22HB_DRIVER_LPS22HB_H_ */

/*! @} */
