/*
 ******************************************************************************
 * @file    lps22hb.c
 * @author  Universal Driver Development Team
 * @brief   LPS22HB driver file
 ******************************************************************************
 * @attention
 *
 * Beautiful license
 *
 ******************************************************************************
 */

/*************** INCLUDE ***************/
#include "lps22hb.h"
#include "lps22hb_core.h"
#include "lps22hb_default.h"

#include <string.h>

/*************** GLOBAL VARIABLE ***************/
lps22hb_communication_t lps22hb_comm;
/**
  * Save the pointer of the write and read function
  * that the user defined. Also modify the config struct
  * of the user with the default variable.
  *
  * @param  setup   user defined setup struct (ptr)
  * @retval       	interface status (MANDATORY: return 0 -> no Error) TODO
  *
  */
void
lps22hb_setup_communication (lps22hb_t * configs)
{
  /* Save pointer to function globally */
  lps22hb_comm.read = configs->comm.read;
  lps22hb_comm.write = configs->comm.write;


  // TODO: Implement return if successful
}

/**
  * Copy the configuration variables from the
  * default configuration file.
  *
  * @param  configs   user defined setup struct (ptr)
  * @retval       	interface status (MANDATORY: return 0 -> no Error) TODO
  *
**/
void
lps22hb_default_settings (lps22hb_t * configs)
{

  configs->config.interrupt_gfc.autorifp = LPS22HB_AUTORIFP_DEF;
  configs->config.interrupt_gfc.reset_arp = LPS22HB_RESET_ARP_DEF;
  configs->config.interrupt_gfc.autozero = LPS22HB_AUTOZERO_DEF;
  configs->config.interrupt_gfc.reset_az = LPS22HB_RESET_AZ_DEF;
  configs->config.interrupt_gfc.diff_en = LPS22HB_DIFF_EN_DEF;
  configs->config.interrupt_gfc.lir = LPS22HB_LIR_DEF;
  configs->config.interrupt_gfc.ple = LPS22HB_PLE_DEF;
  configs->config.interrupt_gfc.phe = LPS22HB_PLE_DEF;

  configs->config.ctrl_reg1.odr = LPS22HB_ODR_DEF;
  configs->config.ctrl_reg1.en_lpfp = LPS22HB_EN_LPFP_DEF;
  configs->config.ctrl_reg1.lpfp_cfg = LPS22HB_LPF_BW_DEF;
  configs->config.ctrl_reg1.bdu = LPS22HB_BDU_DEF;
  configs->config.ctrl_reg1.sim = LPS22HB_SIM_DEF;

  configs->config.ctrl_reg2.boot = LPS22HB_BOOT_DEF;
  configs->config.ctrl_reg2.fifo_en = LPS22HB_FIFO_EN_DEF;
  configs->config.ctrl_reg2.stop_on_fth = LPS22HB_STOP_ON_FTH_DEF;
  configs->config.ctrl_reg2.if_add_inc = LPS22HB_IF_ADD_INC_DEF;
  configs->config.ctrl_reg2.i2c_dis = LPS22HB_I2C_DIS_DEF;
  configs->config.ctrl_reg2.swreset = LPS22HB_SWRESET_DEF;
  configs->config.ctrl_reg2.one_shot = LPS22HB_ONE_SHOT_DEF;

  configs->config.ctrl_reg3.int_h_l = LPS22HB_INT_H_L_DEF;
  configs->config.ctrl_reg3.pp_od = LPS22HB_PP_OD_DEF;
  configs->config.ctrl_reg3.f_fss5 = LPS22HB_F_FSS5_DEF;
  configs->config.ctrl_reg3.f_fth = LPS22HB_F_FTH_DEF;
  configs->config.ctrl_reg3.f_ovr = LPS22HB_F_OVR_DEF;
  configs->config.ctrl_reg3.drdy = LPS22HB_DRDY_DEF;
  configs->config.ctrl_reg3.int_s = LPS22HB_INT_DEF;
}


/**
  * Write the user configurations in the LPS22HB sensor
  *
  * @param  setup   user defined setup struct (ptr)
  * @retval       	interface status (MANDATORY: return 0 -> no Error) TODO
  *
  */
void
lps22hb_setup_sensor (lps22hb_t * configs)
{
  /* Set user configuration */
  set_config_registers (&lps22hb_comm, &configs->config);

}

/**
  * Validate the user configurations in the LPS22HB sensor
  *
  * @param  setup       user defined setup struct (ptr)
  * @retval             lps22hb_err_code_t: error code, LPS22HB_SUCCESS on success
  *
  */
lps22hb_err_code_t
lps22hb_validate_sensor (lps22hb_t * configs)
{
  lps22hb_t readout_configs = { 0 };
  /* Set user configuration */
  get_config_registers (&lps22hb_comm, &readout_configs.config);
  if (compare_configs (&readout_configs.config, &configs->config))
    return LPS22HB_ERR_VALIDATE_SETTINGS;

  return LPS22HB_SUCCESS;
}

/**
  * Setup and validate the user configurations in the LPS22HB sensor
  *
  * @param  setup       user defined setup struct (ptr)
  * @retval             lps22hb_err_code_t: error code, LPS22HB_SUCCESS on success
  *
  */
lps22hb_err_code_t
lps22hb_setup_and_validate_sensor (lps22hb_t * configs)
{
  /* Set user configuration */
  lps22hb_setup_sensor (configs);
  return lps22hb_validate_sensor (configs);
}

/**
  * General register read command
  *
  * @param				uint8_t reg: address to the register
  * @param				uint8_t* reg: ptr to get the read value
  *
  */
void
lps22hb_read_reg (uint8_t reg, uint8_t * value)
{
  lps22hb_comm.read (reg, value, 1);
}

/**
  * General register write command
  *
  * @param				uint8_t reg: address to the register
  * @param				uint8_t* reg: ptr to get the read value
  *
  */
void
lps22hb_write_reg (uint8_t reg, uint8_t * value)
{
  lps22hb_comm.write (reg, value);
}

/**
  * Get the WHO AM I register
  *
  * @param				uint8_t* reg: ptr to get the read value
  *
  */
void
lps22hb_get_who_am_i (uint8_t * value)
{
  lps22hb_comm.read (LPS22HB_WHO_AM_I, value, 1);
}

/**
  * Check the WHO AM I register
  *
  * @retval             lps22hb_err_code_t: error code, LPS22HB_SUCCESS on success
  */
lps22hb_err_code_t
lps22hb_check_who_am_i (void)
{
  uint8_t who_am_i = 0;
  lps22hb_get_who_am_i (&who_am_i);
  if (who_am_i != LPS22HB_WHO_AM_I_VAL)
    return LPS22HB_ERR_WHO_AM_I;
  return LPS22HB_SUCCESS;
}

/**
  * Get the values of the three control registers
  *
  * @param				uint8_t* reg: ptr to get the read value.
  * 					need to point to a buffer of min size 3
  */
void
lps22hb_control_registers (uint8_t * value)
{
  /* initialize reading with the lowest data register, register will be incremented
   * automatically if IF_ADD_INC bit is set. */
  lps22hb_comm.read (LPS22HB_CTRL_REG1, value, 3);
}

/**
  * Get the values of the pressure registers
  *
  * @param				uint8_t* reg: ptr to get the read value.
  * 					need to point to a buffer of min size 3
  */
void
lps22hb_get_raw_pressure (uint8_t * value)
{
  /* initialize reading with the lowest data register, register will be incremented
   * automatically if IF_ADD_INC bit is set. */
  lps22hb_comm.read (LPS22HB_PRESS_OUT_XL, value, 3);
}

/**
  * Get the pressure in hPa
  *
  *  @retval				uint32_t: pressure value. *
  */
uint32_t
lps22hb_get_pressure (void)
{
  /* Read raw data */
  uint8_t pressure_buf[3] = { 0, 0, 0 };
  lps22hb_get_raw_pressure (pressure_buf);

  /* Build 24bit two's complement in 32bit  */
  uint32_t press =
    (((uint32_t) pressure_buf[2]) << 16) | (((uint32_t) pressure_buf[1]) << 8)
    | (uint16_t) pressure_buf[0];
  if (press & 0x00800000)
    press |= 0xFF000000;

  /* Calculate pressure in hPa */
  uint32_t value = ((int32_t) press) / 4096;
  return value;
}

/**
  * Get the values of the pressure reference registers
  *
  * @param				uint8_t* reg: ptr to get the read value.
  * 					need to point to a buffer of min size 3
  */
void
lps22hb_get_pressure_reference (uint8_t * value)
{
  /* initialize reading with the lowest data register, register will be incremented
   * automatically if IF_ADD_INC bit is set. */
  lps22hb_comm.read (LPS22HB_REF_P_XL, value, 3);
}

/**
  * Get the values of the pressure offset registers
  *
  * @param				uint8_t* reg: ptr to get the read value.
  * 					need to point to a buffer of min size 3
  */
void
lps22hb_get_pressure_offset (uint8_t * value)
{
  /* initialize reading with the lowest data register, register will be incremented
   * automatically if IF_ADD_INC bit is set. */
  lps22hb_comm.read (LPS22HB_RPDS_L, value, 2);
}

/**
  * Get the threshold of the pressure
  *
  * @param				uint8_t* reg: ptr to get the read value.
  * 					need to point to a buffer of min size 3
  */
void
lps22hb_get_pressure_threshold (uint8_t * value)
{
  /* initialize reading with the lowest data register, register will be incremented
   * automatically if IF_ADD_INC bit is set. */
  lps22hb_comm.read (LPS22HB_RPDS_L, value, 2);
}

/**
  * Get the values of the temperature registers
  *
  * @param				uint8_t* reg: ptr to get the read value.
  * 					need to point to a buffer of min size 2
  */
void
lps22hb_get_raw_temperature (uint8_t * value)
{
  /* initialize reading with the lowest data register, register will be incremented
   * automatically if IF_ADD_INC bit is set. */
  lps22hb_comm.read (LPS22HB_TEMP_OUT_L, value, 2);
}

/**
  * Get the temperature in °C
  *
  * @retval				int16_t: temperature value.
  */
int16_t
lps22hb_get_temperature (void)
{
  /* Read raw data */
  uint8_t temperature_buf[2] = { 0, 0 };
  lps22hb_get_raw_temperature (temperature_buf);

  /* Bild 16 bit temperature raw value in tsw's complement */
  uint16_t temp =
    (((uint16_t) temperature_buf[1]) << 8) | (uint16_t) temperature_buf[0];

  /* Calculate temperature in  °C */
  int16_t value = (((int16_t) temp) / 100);
  return value;
}
