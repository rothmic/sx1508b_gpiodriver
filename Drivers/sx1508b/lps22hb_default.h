/*
 * lps22hb_default.h
 *
 *  Created on: 03 march 2021
 *      Author: schuluka
 */

#ifndef LPS22HB_LPS22HB_DEFAULT_H_
#define LPS22HB_LPS22HB_DEFAULT_H_

#include "lps22hb.h"

/*---------------------------------------------------------------------------------------------------*/
/*---INTERRUPT_CFG-0x0B------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

/*
 * AUTORIFP
 * 
 * Values:
 *			LPS22HB_ENABLE			AutoRifP enabled
 *			LPS22HB_DISABLE			Normal mode
*/
#define LPS22HB_AUTORIFP_DEF		LPS22HB_DISABLE

/*
 * RESET_ARP
 * 
 * Values:
 *			LPS22HB_ENABLE			Reset AutoRifP function
 *			LPS22HB_DISABLE			Normal mode
*/
#define LPS22HB_RESET_ARP_DEF		LPS22HB_DISABLE

/*
 * AUTOZERO
 * 
 * Values:
 *			LPS22HB_ENABLE			Autozero enabled
 *			LPS22HB_DISABLE			Normal mode
*/
#define LPS22HB_AUTOZERO_DEF		LPS22HB_DISABLE

/*
 * RESET_AZ
 * 
 * Values:
 *			LPS22HB_ENABLE			Reset Autozero function
 *			LPS22HB_DISABLE			Normal mode
*/
#define LPS22HB_RESET_AZ_DEF		LPS22HB_DISABLE

/*
 * DIFF_EN
 * 
 * Values:
 *			LPS22HB_ENABLE			Interrupt generation enabled
 *			LPS22HB_DISABLE			Interrupt generation disabled
*/
#define LPS22HB_DIFF_EN_DEF			LPS22HB_DISABLE

/*
 * LIR
 * 
 * Values:
 *			LPS22HB_ENABLE			Interrupt request latched
 *			LPS22HB_DISABLE			Interrupt request not latched
*/
#define LPS22HB_LIR_DEF				LPS22HB_DISABLE

/*
 * PLE
 * 
 * Values:
 *			LPS22HB_ENABLE			Enable interrupt request on pressure value lower than preset threshold
 *			LPS22HB_DISABLE			Disable interrupt request
*/
#define LPS22HB_PLE_DEF				LPS22HB_DISABLE

/*
 * PHE
 * 
 * Values:
 *			LPS22HB_ENABLE			Enable interrupt request on pressure value higher than preset threshold
 *			LPS22HB_DISABLE			Disable interrupt request
*/
#define LPS22HB_PLE_DEF				LPS22HB_DISABLE

/*---------------------------------------------------------------------------------------------------*/
/*---THS_P_L-0x0C------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

/*
 * User-defined threshold value for pressure interrupt event (lower byte)
 *
 * Values:
 * 			between (and including) 0x00 and 0xFF 
*/
#define LPS22HB_THS_P_L_DEF		0x00

/*---------------------------------------------------------------------------------------------------*/
/*---THS_P_H-0x0D------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

/*
 * User-defined threshold value for pressure interrupt event (upper byte)
 *
 * Values:
 * 			between (and including) 0x00 and 0xFF 
*/
#define LPS22HB_THS_P_H_DEF		0x00

/*---------------------------------------------------------------------------------------------------*/
/*---CTRL_REG1-0x10----------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

/*
 * ODR
 *
 * Values:
 *			LPS22HB_ODR_ONE_SHOT	Output data rate => One-shot 
 *			LPS22HB_ODR_1HZ			Output data rate => 1 Hz 
 *			LPS22HB_ODR_10HZ		Output data rate => 10 Hz
 *			LPS22HB_ODR_25HZ		Output data rate => 25 Hz
 *			LPS22HB_ODR_50HZ		Output data rate => 50 Hz 
 *			LPS22HB_ODR_75HZ		Output data rate => 75 Hz
*/
#define LPS22HB_ODR_DEF				LPS22HB_ODR_1HZ

/*
 * EN_LPFP
 * 
 * Values:
 *			LPS22HB_ENABLE			Low-pass filter enabledd
 *			LPS22HB_DISABLE			Low-pass filter disabled
*/
#define LPS22HB_EN_LPFP_DEF			LPS22HB_DISABLE

/*
 * LPFP_CFG
 *
 * Values:
 *			LPS22HB_LPF_BW_ODR_2	Low-pass filter badwidh = output data rate / 2
 *			LPS22HB_LPF_BW_ODR_9	Low-pass filter badwidh = output data rate / 9
 *			LPS22HB_LPF_BW_ODR_20	Low-pass filter badwidh = output data rate / 20
*/
#define LPS22HB_LPF_BW_DEF			LPS22HB_LPF_BW_ODR_2

/*
 * BDU
 * 
 * Values:
 *			LPS22HB_ENABLE			Output registers not updated until MSB and LSB have been read
 *			LPS22HB_DISABLE			Continuous update
*/
#define LPS22HB_BDU_DEF				LPS22HB_DISABLE

/*
 * SIM
 * 
 * Values:
 *			LPS22HB_ENABLE			SPI 3-wire interface
 *			LPS22HB_DISABLE			SPI 4-wire interface
*/
#define LPS22HB_SIM_DEF				LPS22HB_ENABLE

/*---------------------------------------------------------------------------------------------------*/
/*---CTRL_REG2-0x11----------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

/*
 * BOOT
 * 
 * Values:
 *			LPS22HB_ENABLE			Reboot memory content, self-cleared after BOOT
 *			LPS22HB_DISABLE			Normal mode
*/
#define LPS22HB_BOOT_DEF			LPS22HB_DISABLE

/*
 * FIFO_EN
 * 
 * Values:
 *			LPS22HB_ENABLE			Enable
 *			LPS22HB_DISABLE			Disable
*/
#define LPS22HB_FIFO_EN_DEF			LPS22HB_DISABLE

/*
 * STOP_ON_FTH
 * 
 * Values:
 *			LPS22HB_ENABLE			Enable
 *			LPS22HB_DISABLE			Disable
*/
#define LPS22HB_STOP_ON_FTH_DEF		LPS22HB_DISABLE

/*
 * IF_ADD_INC
 * 
 * Values:
 *			LPS22HB_ENABLE			Enable
 *			LPS22HB_DISABLE			Disable
*/
#define LPS22HB_IF_ADD_INC_DEF		LPS22HB_ENABLE

/*
 * I2C_DIS
 * 
 * Values:
 *			LPS22HB_ENABLE			I2C disabled
 *			LPS22HB_DISABLE			I2C enabled
*/
#define LPS22HB_I2C_DIS_DEF			LPS22HB_ENABLE

/*
 * SWRESET
 * 
 * Values:
 *			LPS22HB_ENABLE			Software rese, self-cleared after reset
 *			LPS22HB_DISABLE			Normal mode
*/
#define LPS22HB_SWRESET_DEF			LPS22HB_DISABLE

/*
 * ONE_SHOT
 * 
 * Values:
 *			LPS22HB_ENABLE			A new dataset is acquired
 *			LPS22HB_DISABLE			Idle mode
*/
#define LPS22HB_ONE_SHOT_DEF		LPS22HB_DISABLE

/*---------------------------------------------------------------------------------------------------*/
/*---CTRL_REG3-0x12----------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

/*
 * INT_H_L
 * 
 * Values:
 *			LPS22HB_ENABLE			Interrupt active low
 *			LPS22HB_DISABLE			Interrupt active high
*/
#define LPS22HB_INT_H_L_DEF			LPS22HB_DISABLE

/*
 * PP_OD
 * 
 * Values:
 *			LPS22HB_ENABLE			Open drain at interrupt pin
 *			LPS22HB_DISABLE			Push-pull at interrupt pin
*/
#define LPS22HB_PP_OD_DEF			LPS22HB_DISABLE

/*
 * F_FSS5
 * 
 * Values:
 *			LPS22HB_ENABLE			Enable
 *			LPS22HB_DISABLE			Disable
*/
#define LPS22HB_F_FSS5_DEF			LPS22HB_DISABLE

/*
 * F_FTH
 * 
 * Values:
 *			LPS22HB_ENABLE			Enable
 *			LPS22HB_DISABLE			Disable
*/
#define LPS22HB_F_FTH_DEF			LPS22HB_DISABLE

/*
 * F_OVR
 * 
 * Values:
 *			LPS22HB_ENABLE			Enable
 *			LPS22HB_DISABLE			Disable
*/
#define LPS22HB_F_OVR_DEF			LPS22HB_DISABLE

/*
 * DRDY
 * 
 * Values:
 *			LPS22HB_ENABLE			Enable
 *			LPS22HB_DISABLE			Disable
*/
#define LPS22HB_DRDY_DEF			LPS22HB_DISABLE

/*
 * INT_S
 *
 * Values:
 *			LPS22HB_INT_DATA		INT_DRDY pin:	Interrupt on data
 *			LPS22HB_INT_PH			INT_DRDY pin:	Interrupt on pressure high
 *			LPS22HB_INT_PL			INT_DRDY pin:	Interrupt on pressure low
 *			LPS22HB_INT_PHL			INT_DRDY pin:	Interrupt on pressure high or low
*/
#define LPS22HB_INT_DEF				LPS22HB_INT_DATA

/*---------------------------------------------------------------------------------------------------*/
/*---FIFO_CTRL-0x14----------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

/*
 * F_MODE
 *
 * Values:
 * 			LPS22HB_FIFO_M_B		FIFO mode:	Bypass 
 * 			LPS22HB_FIFO_M_F		FIFO mode:	FIFO 
 *			LPS22HB_FIFO_M_S		FIFO mode:	Stream
 *			LPS22HB_FIFO_M_STF		FIFO mode:	Stream-to-FIFO 
 *			LPS22HB_FIFO_M_BTS		FIFO mode:	Bypass-to-Stream 
 *			LPS22HB_FIFO_M_DS		FIFO mode:	Dynamic-Stream 
 *			LPS22HB_FIFO_M_BTF		FIFO mode:	Bypass-to-FIFO 
*/
#define LPS22HB_FIFO_MODE_DEF		LPS22HB_FIFO_M_B

/*
 * WTM
 *
 * Values:
 * 			between (and including) 0x00 and 0x1F 
*/
#define LPS22HB_WTM_DEF				0x00

/*---------------------------------------------------------------------------------------------------*/
/*---REF_P_XL-0x15-----------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

/*
 * Reference pressure (lower byte)
 *
 * Values:
 * 			between (and including) 0x00 and 0xFF 
*/
#define LPS22HB_REF_P_XL_DEF		0x00

/*---------------------------------------------------------------------------------------------------*/
/*---REF_P_L-0x16------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

/*
 * Reference pressure (middle byte)
 *
 * Values:
 * 			between (and including) 0x00 and 0xFF 
*/
#define LPS22HB_REF_P_L_DEF			0x00

/*---------------------------------------------------------------------------------------------------*/
/*---REF_P_H-0x17------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

/*
 * Reference pressure (upper byte)
 *
 * Values:
 * 			between (and including) 0x00 and 0xFF 
*/
#define LPS22HB_REF_P_H_DEF			0x00

/*---------------------------------------------------------------------------------------------------*/
/*---RPDS_L-0x18-------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

/*
 * Pressure offset (lower byte)
 *
 * Values:
 * 			between (and including) 0x00 and 0xFF 
*/
#define LPS22HB_RPDS_L_DEF			0x00

/*---------------------------------------------------------------------------------------------------*/
/*---RPDS_H-0x19-------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

/*
 * Pressure offset (upper byte)
 *
 * Values:
 * 			between (and including) 0x00 and 0xFF 
*/
#define LPS22HB_RPDS_H_DEF			0x00

/*---------------------------------------------------------------------------------------------------*/
/*---RES_CONF-0x1A-------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------*/

/*
 * LC_EN
 *
 * Values:
 *			LPS22HB_ENABLE			Low-current mode
 *			LPS22HB_DISABLE			Normal mode (low-noise mode)
*/
#define LPS22HB_LC_EN_DEF			LPS22HB_DISABLE

/*
 * DEFAULT_CONFIG_T
 *
 * Contains the default struct config
 *
*/
/*
 * DEFAULT_CONFIG_T
 *
 * Contains the default struct config
 *
*/

#define LPS22HB_DEFAULT_CONFIG 								 	\
{																 	\
	.config.interrupt_gfc.autorifp = LPS22HB_AUTORIFP_DEF,   	\
	.config.interrupt_gfc.reset_arp = LPS22HB_RESET_ARP_DEF, 	\
	.config.interrupt_gfc.autozero = LPS22HB_AUTOZERO_DEF,   	\
	.config.interrupt_gfc.reset_az = LPS22HB_RESET_AZ_DEF,   	\
	.config.interrupt_gfc.diff_en = LPS22HB_DIFF_EN_DEF,     	\
	.config.interrupt_gfc.lir = LPS22HB_LIR_DEF,             	\
	.config.interrupt_gfc.ple = LPS22HB_PLE_DEF,             	\
	.config.interrupt_gfc.phe = LPS22HB_PLE_DEF,             	\
                                                            		\
	.config.ctrl_reg1.odr = LPS22HB_ODR_DEF,                 	\
	.config.ctrl_reg1.en_lpfp = LPS22HB_EN_LPFP_DEF,         	\
	.config.ctrl_reg1.lpfp_cfg = LPS22HB_LPF_BW_DEF,         	\
	.config.ctrl_reg1.bdu = LPS22HB_BDU_DEF,                 	\
	.config.ctrl_reg1.sim = LPS22HB_SIM_DEF,                 	\
                                                            		\
	.config.ctrl_reg2.boot = LPS22HB_BOOT_DEF,               	\
	.config.ctrl_reg2.fifo_en = LPS22HB_FIFO_EN_DEF,         	\
	.config.ctrl_reg2.stop_on_fth = LPS22HB_STOP_ON_FTH_DEF, 	\
	.config.ctrl_reg2.if_add_inc = LPS22HB_IF_ADD_INC_DEF,   	\
	.config.ctrl_reg2.i2c_dis = LPS22HB_I2C_DIS_DEF,         	\
	.config.ctrl_reg2.swreset = LPS22HB_SWRESET_DEF,         	\
	.config.ctrl_reg2.one_shot = LPS22HB_ONE_SHOT_DEF,       	\
                                                            		\
	.config.ctrl_reg3.int_h_l = LPS22HB_INT_H_L_DEF,         	\
	.config.ctrl_reg3.pp_od = LPS22HB_PP_OD_DEF,             	\
	.config.ctrl_reg3.f_fss5 = LPS22HB_F_FSS5_DEF,           	\
	.config.ctrl_reg3.f_fth = LPS22HB_F_FTH_DEF,             	\
	.config.ctrl_reg3.f_ovr = LPS22HB_F_OVR_DEF,             	\
	.config.ctrl_reg3.drdy = LPS22HB_DRDY_DEF,               	\
	.config.ctrl_reg3.int_s = LPS22HB_INT_DEF,               	\
}

#endif /* LPS22HB_LPS22HB_DEFAULT_H_ */
